package com.xcc.shopping.dao;

import com.xcc.shopping.entity.ProductDescript;
import com.xcc.shopping.entity.ProductInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface ProductDescriptDao {

    @Insert("insert into product_descript(product_info_id,descript,store_info_id) " +
            "values(#{productInfoId},#{descript},#{storeInfoId})")
    @Options(useGeneratedKeys = true,keyProperty="id",keyColumn = "id")
    int insert(ProductDescript productDescript);


}
