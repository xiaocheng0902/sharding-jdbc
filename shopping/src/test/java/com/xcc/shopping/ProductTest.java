package com.xcc.shopping;


import com.xcc.shopping.dao.ProductInfoDao;
import com.xcc.shopping.entity.ProductInfo;
import com.xcc.shopping.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
public class ProductTest {

    @Autowired
    ProductService productService;
    @Autowired
    ProductInfoDao productInfoDao;

    @Test
    public void testProductInsert() {
        for (int i = 1; i < 10; i++) {
            ProductInfo productInfo = new ProductInfo();
            productInfo.setStoreInfoId(1L);//店铺id

            productInfo.setProductName("Java编程思想" + i);//商品名称
            productInfo.setSpec("大号");
            productInfo.setPrice(new BigDecimal(60));
            productInfo.setRegionCode("110100");
            productInfo.setDescript("Java编程思想不错！！！" + i);//商品描述
            productInfo.setImageUrl("images"+i);
            productService.insert(productInfo);
        }

    }


    //查询商品
    @Test
    public void testQueryProduct(){
        //实际上会查 start+pageSize条数据
    //select i.*,d.descript,r.region_name placeOfOrigin from product_info_2 i join product_descript_2 d on i.product_info_id = d.product_info_id  join region r on i.region_code = r.region_code order by product_info_id desc limit ?,? ::: [0, 4]
        List<ProductInfo> productInfos = productInfoDao.selectProductList(2, 5);
        System.out.println(productInfos);
    }

    //统计商品总数
    @Test
    public void testSelectCount(){

        int i = productInfoDao.selectCount();

        System.out.println(i);
    }

    //分组统计商品
    @Test
    public void testSelectProductGroupList(){

        List<Map> maps = productInfoDao.selectProductGroupList();

        System.out.println(maps);
    }

}
