package com.xcc.dbsharding;

import com.google.common.collect.Lists;
import com.xcc.dbsharding.bean.OrderBean;
import com.xcc.dbsharding.dao.OrderDAO;
import com.xcc.dbsharding.vo.UserOrderVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJDBCApplication.class})
public class OrderTest {

    @Autowired
    OrderDAO orderDAO;

    @Test
    public void testInsert() {
        for (int i = 0; i < 10; i++) {
            orderDAO.insertOrder(new BigDecimal(10),2,"1");
        }
    }

    @Test
    public void testSelectByIds(){
        //此时两个数都order_id均为奇数，故而只需要查t_order_2表
        List<Map<String, Object>> maps = orderDAO.selectByOrderIds(Lists.newArrayList(560213547276042241L, 560213547703861249L));
        //order_id为1奇1偶，故而t_order_1和t_order_2均要查
//        List<Map<String, Object>> maps = orderDAO.selectByOrderIds(Lists.newArrayList(560190619398438913L, 560190619876589568L));
        System.out.println(maps);
    }

    @Test
    public void testSelectByUserId() {
        //由于并不是以user_id作为分片键的，故而两张表都要查
        List<OrderBean> maps = orderDAO.selectByuserId(1);
        System.out.println(maps);
    }

    @Test
    public void testUserOrder() {
        ArrayList<Long> orderIds = Lists.newArrayList(560213547276042241L, 560213547703861249L);
        List<UserOrderVo> userOrderVos = orderDAO.selectUserOrderByOrderIds(orderIds);
        System.out.println(userOrderVos);
    }


}
