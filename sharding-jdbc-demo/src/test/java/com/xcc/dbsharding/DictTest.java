package com.xcc.dbsharding;

import com.google.common.collect.Lists;
import com.xcc.dbsharding.bean.OrderBean;
import com.xcc.dbsharding.dao.DictDAO;
import com.xcc.dbsharding.dao.OrderDAO;
import com.xcc.dbsharding.vo.UserOrderVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJDBCApplication.class})
public class DictTest {

    @Autowired
    DictDAO dictDao;

    @Test
    public void testInsertDict() {
        dictDao.insertDict(1L, "user_type", "0", "管理员");
        dictDao.insertDict(2L, "user_type", "1", "操作员");
    }

    @Test
    public void testDeleteDict() {
        dictDao.deleteDict(1L);
        dictDao.deleteDict(2L);
    }


}
