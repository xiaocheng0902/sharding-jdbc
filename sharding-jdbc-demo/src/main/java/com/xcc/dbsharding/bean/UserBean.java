package com.xcc.dbsharding.bean;

import lombok.Data;

@Data
public class UserBean {

    private long userId;
    private String fullname;
    private String userType;

}
